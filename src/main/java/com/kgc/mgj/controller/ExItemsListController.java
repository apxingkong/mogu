package com.kgc.mgj.controller;

import com.kgc.mgj.service.ExItemsListService;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.vo.ExItemsVo;
import com.kgc.mgj.vo.ExItemtypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "商品首页")
@RestController
@RequestMapping(value = "MoGu")
public class ExItemsListController {
    @Autowired
    private ExItemsListService exItemsListService;

    @ApiOperation(value = "商品查询")
    @GetMapping(value = "list")
    public PageUtils<ExItemsVo> select(@RequestParam(required = false) String itemName, @RequestParam(required = false) Integer itemType,
                                       @RequestParam(required = false) Integer pageNo, @RequestParam(required = false) Integer pageSize) {
        PageUtils<ExItemsVo> pageUtils = exItemsListService.select(itemName, itemType, pageNo, pageSize);
        return pageUtils;
    }

    @ApiOperation(value = "商品类型查询")
    @GetMapping(value = "type")
    public List<ExItemtypeVo> selcetType(Integer Id) {
        List<ExItemtypeVo> exItemtypeVos = exItemsListService.selectType(Id);
        return exItemtypeVos;
    }
}