package com.kgc.mgj.controller;

import com.kgc.mgj.config.api.CurrentUser;
import com.kgc.mgj.config.api.LoginRequired;
import com.kgc.mgj.service.BankService;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author jialin
 * @create 2020-10-05
 */
@Log4j
@Api(tags = "银行卡绑定接口")
@RestController
@RequestMapping(value = "/bank")
public class BankController {

    @Autowired
    private BankService bankService;

    @LoginRequired
    @ApiOperation("银行卡绑定接口")
    @GetMapping(value = "/bindBank")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 510, message = "银行卡绑定异常"),
            @ApiResponse(code = 512, message = "手机验证码错误")
    })
    public ReturnResult bindBank(@ApiParam(value = "用户姓名") @RequestParam String userName, @ApiParam(value = "身份证号码") @RequestParam String card, @ApiParam(value = "银行卡号") @RequestParam String bankCode, @ApiParam(value = "手机验证码") @RequestParam String phoneCode, @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        Integer returnCode = bankService.BindBank(userId, card, bankCode, userName, phoneCode);
        if (returnCode == 1){
            return ReturnResultUtils.returnFailMsg(510, "银行卡绑定异常");
        }else if (returnCode == 2){
            return ReturnResultUtils.returnFailMsg(512, "手机验证码错误");
        }
        return ReturnResultUtils.returnSuccess("银行卡绑定成功！");
    }

    @LoginRequired
    @ApiOperation("短信发送接口")
    @GetMapping(value = "/sendMsg")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 511, message = "短信发送接口异常")
    })
    public ReturnResult sendMsg(@ApiParam(value = "电话") @RequestParam String phone, @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            bankService.getPhoneCode(userId, phone);
            return ReturnResultUtils.returnSuccess("短信发送成功！");
        } catch (Exception e) {
            log.error("短信发送接口异常：" + e);
            return ReturnResultUtils.returnFailMsg(510, "短信发送接口异常！");
        }
    }

}
