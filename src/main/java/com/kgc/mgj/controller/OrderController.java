package com.kgc.mgj.controller;

import com.kgc.mgj.config.api.CurrentUser;
import com.kgc.mgj.config.api.LoginRequired;
import com.kgc.mgj.params.QueryOrderParam;
import com.kgc.mgj.service.OrderService;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import com.kgc.mgj.vo.OrderListVo;
import com.kgc.mgj.vo.OrderPayedListVo;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author jialin
 * @create 2020-10-01
 */
@Api(tags = "订单接口")
@RestController
@RequestMapping(value = "/order")
@Log4j
public class OrderController {

    @Autowired
    private OrderService orderService;

    @LoginRequired
    @ApiOperation(value = "查询订单接口", notes = "展示各种tab下的订单，可以选择各种tab")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 513, message = "查询订单失败")
    })
    @PostMapping(value = "/queryAllPayed")
    public ReturnResult<OrderPayedListVo> queryAllPayed(@ApiParam(required = true) @RequestBody QueryOrderParam queryOrderParam, @ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize, @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            PageUtils<OrderPayedListVo> orderPayedListVoPageUtils = orderService.queryAllItemsPerOrder(queryOrderParam, userId, pageNo, pageSize);
            return ReturnResultUtils.returnSuccess(orderPayedListVoPageUtils);
        } catch (Exception e) {
            log.error("查询订单失败！" + e);
            return ReturnResultUtils.returnFailMsg(513, "查询订单失败");
        }
    }

    @LoginRequired
    @ApiOperation(value = "删除订单", notes = "删除订单")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 502, message = "删除订单异常")
    })
    @GetMapping(value = "/delOrder")
    public ReturnResult delOrder(@ApiParam(value = "订单编号") @RequestParam String orderId, @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        if (!orderService.deleteOrder(orderId, userId)) {
        return ReturnResultUtils.returnFailMsg(502, "删除订单失败！");
        }
        return ReturnResultUtils.returnSuccess("删除成功！");
    }

    @LoginRequired
    @ApiOperation(value = "生成未支付订单", notes = "生成未支付订单，要先调用查询收货地址的接口，传一个收货地址")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 411, message = "生成未支付订单异常"),
    })
    @PostMapping(value = "/createOrder")
    public ReturnResult createOrder(@ApiParam(value = "收货地址") @RequestParam String address, @ApiIgnore @CurrentUser String openid, @ApiParam(value = "购物车中所有商品ID") @RequestParam String... itemIds) {
        String userId = openid;
        try {
            orderService.createOrder(address, userId, itemIds);
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("生成未支付订单发生异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(411, "生成未支付订单异常");
    }

}
