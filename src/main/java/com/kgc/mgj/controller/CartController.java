package com.kgc.mgj.controller;

import com.kgc.mgj.config.api.CurrentUser;
import com.kgc.mgj.config.api.LoginRequired;
import com.kgc.mgj.dto.ItemCartDto;
import com.kgc.mgj.params.AddToCartItemParam;
import com.kgc.mgj.params.ModifyItemCountParam;
import com.kgc.mgj.service.CartService;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import com.kgc.mgj.vo.CartPageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 13:57
 */
@Api(tags = "购物车模块")
@Slf4j
@RestController
@RequestMapping("/cart")
public class CartController {
    @Resource
    private CartService cartService;

    /**
     * 这个方法，将返回用户点击 “购物车” 后展示的信息
     *
     *
     */
    @LoginRequired
    @ApiOperation("获取当前用户的购物车")
    @GetMapping("/getCart")
    public ReturnResult<CartPageVo> getCart(@ApiIgnore @CurrentUser String openid) {
        try {
//            String userId = wxUserVo.getOpenid();
            CartPageVo cartPageVo = cartService.getCartPage(openid);
            return ReturnResultUtils.returnSuccess(cartPageVo);
        } catch (Exception e) {
            log.error("查询购物车发生异常", e);
            return ReturnResultUtils.returnFailMsg(599, "查询用户购物车时发生异常");
        }
    }

    @LoginRequired
    @ApiOperation("将商品添加入购物车")
    @PutMapping("/addToCart")
    public ReturnResult addToCart(@ApiParam(required = true) @RequestBody AddToCartItemParam addToCartItemParam,@ApiIgnore @CurrentUser String openid) {
        try {
            //使用前端数据初步构建入库的实体模型
            ItemCartDto itemCartDto = new ItemCartDto();
            itemCartDto.setItemId(addToCartItemParam.getItemId());
            itemCartDto.setItemCount(addToCartItemParam.getNum());
            itemCartDto.setShopTypeName(addToCartItemParam.getShopTypeName());
            itemCartDto.setCouponId(addToCartItemParam.getCouponId());
            itemCartDto.setUserId(openid);

            //进行逻辑处理，并且入库
            int addRes = cartService.addItemToCart(itemCartDto);
            switch (addRes) {
                case 100:
                    return ReturnResultUtils.returnSuccess();
                case 101:
                    return ReturnResultUtils.returnFailMsg(597, "库存不足");
            }
            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            return ReturnResultUtils.returnFailMsg(598, "添加购物车时发生异常");
        }
    }

    @LoginRequired
    @ApiOperation("修改购物车中商品的选购数量")
    @PostMapping("/modifyItemCount")
    public ReturnResult modifyItemCount(@ApiParam(required = true) @RequestBody ModifyItemCountParam modifyItemCountParam,@ApiIgnore @CurrentUser String openid) {
        try {
            ItemCartDto itemCartDto = new ItemCartDto();
            itemCartDto.setUserId(openid);
            itemCartDto.setItemId(modifyItemCountParam.getItemId());
            itemCartDto.setItemCount(modifyItemCountParam.getItemCount());
            int res = cartService.modifyItemCount(itemCartDto);

            switch (res) {
                case 100:
                    return ReturnResultUtils.returnSuccess();
                case 101:
                    return ReturnResultUtils.returnFailMsg(595, "无法修改，库存不足");
            }

            return ReturnResultUtils.returnSuccess();
        } catch (Exception e) {
            log.error("修改购物车商品数量发生异常");
            return ReturnResultUtils.returnFailMsg(596, "修改购物车商品数量发生异常");
        }
    }

    @LoginRequired
    @ApiOperation("从购物车中移除商品")
    @DeleteMapping("/removeItemFromCart")
    public ReturnResult removeItemFromCart(@ApiIgnore @CurrentUser String openid, @ApiParam(value = "要移除的商品ids", required = true) @RequestBody String... itemIds) {
        try {
            if(itemIds.length == 0 ) return ReturnResultUtils.returnFailMsg(592, "没有提交要删除的商品列表");

            int lines = cartService.removeItemFromCart(openid, itemIds);
            if (lines>0) return ReturnResultUtils.returnSuccess();

            else return ReturnResultUtils.returnFailMsg(593, "删除了0行数据");
        } catch (Exception e) {
            return ReturnResultUtils.returnFailMsg(594, "删除商品中发生异常");
        }
    }
    
}
