package com.kgc.mgj.controller;

import com.kgc.mgj.config.api.CurrentUser;
import com.kgc.mgj.config.api.LoginRequired;
import com.kgc.mgj.service.TraceService;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import com.kgc.mgj.vo.TraceVo;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author jialin
 * @create 2020-10-08
 */
@Api(tags = "足迹接口")
@RestController
@RequestMapping(value = "/trace")
@Log4j
public class TraceController {

    @Autowired
    private TraceService traceService;

    @LoginRequired
    @ApiOperation(value = "获取足迹", notes = "足迹（显示全部，服饰，等tab并且显示数量，显示查看商品的具体时间单位为天）")
    @GetMapping(value = "/getTraces")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 501, message = "获取足迹异常")
    })
    public ReturnResult<TraceVo> getTraces(@ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid){
        String userId = openid;
        try {
            List<TraceVo> traceVos = traceService.queryTraces(userId);
            return ReturnResultUtils.returnSuccess(traceVos);
        }catch (Exception e){
            return ReturnResultUtils.returnFailMsg(505,"获取足迹异常");
        }
    }

}
