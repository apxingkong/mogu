package com.kgc.mgj.controller;

import com.kgc.mgj.config.api.CurrentUser;
import com.kgc.mgj.config.api.LoginRequired;
import com.kgc.mgj.service.*;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import com.kgc.mgj.vo.*;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by XuHui on 2020/9/21
 */
@Api(tags = "商品详情页")
@Log4j
@RestController
@RequestMapping(value = "/GoodsDetail")
public class GoodsDetailController {
    @Autowired
    private ItemService itemService;
    @Autowired
    private StockService stockService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;

    @LoginRequired
    @ApiOperation(value = "查询商品详情", notes = "查询商品详情")
    @GetMapping(value = "/getGoods")
    public ReturnResult<ExItemsVo> getGoods(@ApiParam(value = "商品ID", required = true) @RequestParam String itemId, @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            ExItemsVo exItemsVo = itemService.queryGoods(itemId, userId);
            return ReturnResultUtils.returnSuccess(exItemsVo);
        } catch (Exception e) {
            log.error("查询商品出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(405, "查询商品异常");
    }

    @ApiOperation(value = "查询商品库存详情", notes = "查询商品库存详情")
    @GetMapping(value = "/getStock")
    public ReturnResult<ExItemRepoVo> getStock(@ApiParam(value = "商品ID", required = true) @RequestParam String itemId) {
        try {
            ExItemRepoVo exItemRepoVo = stockService.getStock(itemId);
            return ReturnResultUtils.returnSuccess(exItemRepoVo);
        } catch (Exception e) {
            log.error("查询库存出现异常：" + e);
        }
        return ReturnResultUtils.returnFailMsg(407, "查询库存异常");
    }

    @LoginRequired
    @ApiOperation(value = "查询用户个人信息",notes = "用户信息")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 505, message = "查询失败")
    })
    @GetMapping(value = "/queryUser")
    public ReturnResult<UserVo> queryUser(@ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String userId){
        try {
            UserVo userVo1 = userService.queryUser(userId);
            return ReturnResultUtils.returnSuccess(userVo1);
        }catch (Exception e){
            log.error("查询用户个人信息失败！" + e);
            return ReturnResultUtils.returnFailMsg(505, "查询失败！");
        }
    }


    @ApiOperation(value = "查询评论详情", notes = "查询评论详情")
    @GetMapping(value = "/getComment")
    public ReturnResult<CommentVo> getComment(@ApiParam(value = "第几页") @RequestParam(required = false) Integer pageNo, @ApiParam(value = "查几条") @RequestParam(required = false) Integer pageSize,
                                              @ApiParam(value = "商品ID", required = true) @RequestParam String itemId) {
        try {
            PageUtils<CommentVo> commentVos = commentService.getComment(pageNo, pageSize, itemId);
            return ReturnResultUtils.returnSuccess(commentVos);
        } catch (Exception e) {
            log.error("查询评论出现异常：" + e);
            return ReturnResultUtils.returnFailMsg(409, "查询评论异常");
        }
    }

}
