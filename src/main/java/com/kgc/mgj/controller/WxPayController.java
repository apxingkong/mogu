package com.kgc.mgj.controller;

import com.kgc.mgj.config.api.CurrentUser;
import com.kgc.mgj.config.api.LoginRequired;
import com.kgc.mgj.service.OrderService;
import com.kgc.mgj.service.WxPayService;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author jialin
 * @createTime 2020-09-26
 */
@Log4j
@Api(tags = "微信支付接口")
@Controller
@RequestMapping(value = "/wxPay")
public class WxPayController {
    
    @Autowired
    private WxPayService wxPayService;

    @Autowired
    private OrderService orderService;

    @LoginRequired
    @ApiOperation("微信支付")
    @GetMapping(value = "/getCodeUrl")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有货页面跳转路径不对"),
            @ApiResponse(code = 411, message = "生成已支付订单异常"),
            @ApiResponse(code = 412, message = "库存不足"),
            @ApiResponse(code = 413, message = "余额不足"),
            @ApiResponse(code = 205, message = "生成未支付订单")
    })
    public ReturnResult getCodeUrl(@ApiParam(value = "收货地址") @RequestParam String address, @ApiIgnore @CurrentUser String openid, @ApiParam(value = "购物车中所有商品ID") @RequestParam String... itemIds) throws Exception {
        String str = wxPayService.unifiedOrder();
        if (!StringUtils.isEmpty(str)){
            String userId = openid;
            try {
                int result = orderService.createOrderPayed(address, userId, itemIds);
                if (result == 200){
                    return ReturnResultUtils.returnSuccess("生成已支付订单");
                }else if (result == 301){
                    return ReturnResultUtils.returnFailMsg(412, "库存不足");
                }else if (result == 302){
                    orderService.createOrder(address, userId, itemIds);
                    return ReturnResultUtils.returnFailMsg(413, "余额不足!生成未支付订单！");
                }
            } catch (Exception e) {
                log.error("生成已支付订单发生异常：" + e);
                orderService.createOrder(address, userId, itemIds);
                return ReturnResultUtils.returnNoSuccess("支付失败！生成未支付订单！");
            }
        }
        return ReturnResultUtils.returnFailMsg(411, "生成已支付订单异常");
    }
}
