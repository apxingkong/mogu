package com.kgc.mgj.controller;

import com.kgc.mgj.config.api.CurrentUser;
import com.kgc.mgj.config.api.LoginRequired;
import com.kgc.mgj.service.CollectionService;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import com.kgc.mgj.vo.CollectionVo;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author jialin
 * @create 2020-10-07
 */
@Log4j
@Api(tags = "收藏接口")
@RestController
@RequestMapping(value = "/collection")
public class CollectionController {

    @Autowired
    private CollectionService collectionService;

    @LoginRequired
    @ApiOperation(value = "收藏列表", notes = "店铺logo，上新个数（基于昨天今天新上的），上新时间，上新的商品（图片，名字，价格，更新时间），有且最多10个上新，可以收藏多个店铺")
    @GetMapping(value = "/getCollection")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 501, message = "获取收藏列表异常")
    })
    public ReturnResult getCollection(@ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            List<CollectionVo> collectionVos = collectionService.getCollection(userId);
            return ReturnResultUtils.returnSuccess(collectionVos);
        } catch (Exception e) {
            return ReturnResultUtils.returnFailMsg(501, "获取收藏列表异常");
        }
    }

    @LoginRequired
    @ApiOperation(value = "添加收藏", notes = "登录后输入店铺id")
    @PostMapping(value = "/addCollection")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 502, message = "添加收藏异常")
    })
    public ReturnResult addCollection(@ApiParam(value = "店铺id") @RequestParam Integer shopId, @ApiParam(value = "登录后的用户对象") @ApiIgnore @CurrentUser String openid) {
        String userId = openid;
        try {
            collectionService.addCollection(shopId, userId);
            return ReturnResultUtils.returnSuccess("添加收藏成功！");
        } catch (Exception e) {
            return ReturnResultUtils.returnFailMsg(502, "添加收藏异常！");
        }

    }

}
