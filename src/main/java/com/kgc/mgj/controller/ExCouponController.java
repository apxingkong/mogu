package com.kgc.mgj.controller;

import com.kgc.mgj.dto.ExCoupon;
import com.kgc.mgj.service.ExCouponService;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.utils.ReturnResult;
import com.kgc.mgj.utils.ReturnResultUtils;
import com.kgc.mgj.vo.ExCouponVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jialin
 * @create 2020-10-04
 */
@Log4j
@Api(tags = "优惠券练习接口1.0")
@RestController
@RequestMapping("/coupon")
public class ExCouponController {

    @Autowired
    private ExCouponService exCouponService;

    @ApiOperation(value = "选择最优优惠券", notes = "选择最优优惠券")
    @GetMapping(value = "/chooseBestCoupon")
    public ReturnResult chooseBestCoupon(@RequestParam String itemId, @RequestParam Integer itemCount) {
        try {
            String couponId = exCouponService.chooseBestCoupon(itemId, itemCount);
            return ReturnResultUtils.returnSuccess(couponId);
        } catch (Exception e){
            log.error("选择最优优惠券失败!" + e);
            return ReturnResultUtils.returnFailMsg(541, "选择最优优惠券失败！");
        }
    }

    @ApiOperation(value = "添加优惠券信息", notes = "添加优惠券")
    @PostMapping(value = "/addCoupons")
    public ReturnResult addCoupons(@RequestBody ExCoupon exCoupon) {
        if (exCouponService.addExCoupon(exCoupon)) {
            return ReturnResultUtils.returnSuccess();
        } else {
            return ReturnResultUtils.returnFailMsg(508, "添加优惠券失败！");
        }
    }

    @ApiOperation(value = "删除优惠券信息", notes = "可以多个删除")
    @PostMapping(value = "/delCoupons")
    public ReturnResult delCoupons(@RequestParam String... ids) {
        if (exCouponService.deleteExCoupon(ids)) {
            return ReturnResultUtils.returnSuccess();
        } else {
            return ReturnResultUtils.returnFailMsg(507, "删除优惠券失败！");
        }
    }

    @ApiOperation(value = "修改优惠券信息", notes = "修改优惠券")
    @PostMapping(value = "/modCoupons")
    public ReturnResult modCoupons(@RequestBody ExCoupon exCoupon) {
        if (exCouponService.modifyExCoupon(exCoupon)) {
            return ReturnResultUtils.returnSuccess();
        } else {
            return ReturnResultUtils.returnFailMsg(509, "修改优惠券信息失败！");
        }
    }

    @ApiOperation(value = "查询优惠券信息", notes = "分页，多条件查询")
    @PostMapping(value = "/queryCoupons")
    public ReturnResult<ExCouponVo> queryCoupons(@RequestBody ExCoupon exCoupon, @RequestParam int pageNo, @RequestParam int pageSize) {
        PageUtils<ExCouponVo> exCouponVoList = exCouponService.queryExCoupon(exCoupon, pageNo, pageSize);
        return ReturnResultUtils.returnSuccess(exCouponVoList);
    }

    @ApiOperation(value = "根据商品id查询有效优惠券", notes = "输入商品id")
    @GetMapping(value = "/queryCouponsByItemId")
    @ApiResponses({
            @ApiResponse(code = 401, message = "未经授权"),
            @ApiResponse(code = 403, message = "被禁止"),
            @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对"),
            @ApiResponse(code = 521, message = "根据商品id查询优惠券失败")
    })
    public ReturnResult<ExCouponVo> queryCouponsByItemId(@RequestParam String itemId) {
        try {
            List<ExCouponVo> exCouponVoList = exCouponService.queryCoupon(itemId);
            return ReturnResultUtils.returnSuccess(exCouponVoList.size() > 0 ? exCouponVoList : "暂无优惠券");
        }catch (Exception e){
            log.error("根据商品id查询优惠券失败！" + e);
            return ReturnResultUtils.returnFailMsg(521,"根据商品id查询优惠券失败!");
        }
    }
}
