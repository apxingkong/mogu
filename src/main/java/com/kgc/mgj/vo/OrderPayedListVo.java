package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 已完成支付的订单列表VO
 * @author jialin
 * @createTime 2020-09-21
 */
@Data
@ApiModel("订单列表VO")
public class OrderPayedListVo implements Serializable {

    @ApiModelProperty("用户编号")
    private String userId;

    @ApiModelProperty("订单ID")
    private String orderId;

    @ApiModelProperty("商品编号")
    private String itemId;

    @ApiModelProperty("商品主图")
    private String picMain;

    @ApiModelProperty("商品名称")
    private String itemName;

    @ApiModelProperty("店铺名称")
    private String shopTypeName;

    @ApiModelProperty("商品价格")
    private double itemPrice;

    @ApiModelProperty("商品数量")
    private Integer itemCount;

    @ApiModelProperty( "商品合计价格(考虑折扣优惠)")
    private double itemTotalprice;

    @ApiModelProperty("商品主题（0：上衣，1：裙装，2：短裤，3:女鞋...）")
    private String itemType;

    @ApiModelProperty("支付状态(0：未支付，1：已支付)")
    private String ifPay;

    @ApiModelProperty("是否超时(0：未超时，1：已超时)")
    private String orderOvertime;

    @ApiModelProperty("是否删除(0：未删除，1：已删除)假删除")
    private String orderIfdel;

    @ApiModelProperty("是否收货(o未收货/1已收货)")
    private String orderIfreceive;

    @ApiModelProperty("收货地址")
    private String address;

}
