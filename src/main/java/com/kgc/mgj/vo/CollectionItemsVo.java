package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author jialin
 * @create 2020-10-07
 */
@Data
@ApiModel("页面商品VO")
public class CollectionItemsVo implements Serializable {

    @ApiModelProperty("商品id")
    private String itemId;

    @ApiModelProperty("商品名称")
    private String itemName;

    @ApiModelProperty("商品主图")
    private String picMain;

    @ApiModelProperty("商品价格")
    private double itemPrice;

    @ApiModelProperty("上架时间")
    private String ItemCreateTime;

}
