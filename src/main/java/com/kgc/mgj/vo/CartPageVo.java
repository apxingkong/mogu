package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 19:42
 */
@Data
@ApiModel("购物车页面模型")
public class CartPageVo implements Serializable {
    @ApiModelProperty("地址信息")
    private String address;
    @ApiModelProperty("商品信息")
    private List<ItemCartVo> itemCartVos;
    @ApiModelProperty("已使用优惠券的数量")
    private Integer usedCouponNum;
    @ApiModelProperty("已使用优惠券的优惠金额")
    private double discountPrice;
    @ApiModelProperty("合计多少钱")
    private double totalPrice;
    @ApiModelProperty("购物车中一共有多少商品")
    private Integer itemNum;
}
