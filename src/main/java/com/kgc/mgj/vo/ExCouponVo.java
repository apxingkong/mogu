package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "优惠券Vo")
public class ExCouponVo implements Serializable{

    @ApiModelProperty(value = "商品名称")
    private String itemName;

    @ApiModelProperty(value = "优惠券编号")
    private String couponId;

    @ApiModelProperty(value = "优惠券标题")
    private String couponTitle;

    @ApiModelProperty(value = "优惠券类型(0：全场，1：店铺)")
    private Integer type;

    @ApiModelProperty(value = "满多少金额")
    private Double withAmount;

    @ApiModelProperty(value = "用券金额")
    private Double usedAmount;

    @ApiModelProperty(value = "使用开始时间")
    private String startTime;

    @ApiModelProperty(value = "有效期")
    private Integer valueTime;

}