package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class ExItemsVo {


    /**
     * Database Column Remarks:
     *   商品名称

     */
    @ApiModelProperty("商品名称")
    private String itemName;

    /**
     * Database Column Remarks:
     *   商品描述

     */
    @ApiModelProperty("商品描述")
    private String itemDetail;

    /**
     * Database Column Remarks:
     *   商品主图

     */
    @ApiModelProperty("商品主图")
    private String picMain;

    @ApiModelProperty("商品详情图")
    private String picDetail;

    /**
     * Database Column Remarks:
     *   商品颜色(某些商品没有就空着)
     */
    @ApiModelProperty("商品颜色")
    private Integer itemColortype;

    /**
     * Database Column Remarks:
     *   商品尺码(某些商品没有就空着)

     */
    @ApiModelProperty("商品尺码")
    private Integer itemSizetype;

    /**
     * Database Column Remarks:
     *   商品原价

     */
    @ApiModelProperty("商品原价")
    private Double itemPrice;

    /**
     * Database Column Remarks:
     *   商品折扣价

     */
    @ApiModelProperty("商品折扣价")
    private Double discountPrice;


    @ApiModelProperty("商品主题")
    private Integer itemType;

    @ApiModelProperty("热门商品")
    private Integer brandType;


    @ApiModelProperty("店铺编号")
    private String shopId;





}
