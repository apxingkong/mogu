package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chutao
 * @since 2020-10-07
 */
@Data
@ApiModel("用户个人中心Vo")
public class UserVo {

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户头像")
    private String userPic;

    @ApiModelProperty("用户昵称")
    private String userName;

    @ApiModelProperty("电话号码")
    private String userPhone;

    @ApiModelProperty("用户密码")
    private String userPwd;

    @ApiModelProperty("用户余额")
    private Double userBalance;

    @ApiModelProperty("待支付数")
    private Integer toPayCount;

    @ApiModelProperty("优惠券数")
    private Integer couponCount;

}
