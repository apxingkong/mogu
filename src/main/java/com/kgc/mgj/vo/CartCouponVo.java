package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/23 14:26
 */
@Data
@ApiModel("传递给购物车页面的优惠券模型")
public class CartCouponVo implements Serializable {
    @ApiModelProperty("优惠券id")
    private String couponId;
    @ApiModelProperty("优惠券的生效时间：毫秒值")
    private long startTime;
    @ApiModelProperty("优惠券的有效期：天")
    private int valueTime;
    @ApiModelProperty("优惠金额")
    private double usedAmount;

}
