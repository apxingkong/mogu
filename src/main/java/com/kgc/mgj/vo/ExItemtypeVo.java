package com.kgc.mgj.vo;

import lombok.Data;

@Data
public class ExItemtypeVo {

    /**
     * Database Column Remarks:
     *   商品类型

     * @mbg.generated
     */
    private String typeName;

}