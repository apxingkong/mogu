package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 订单列表VO
 * @author jialin
 * @createTime 2020-09-21
 */
@Data
@ApiModel("订单列表VO")
public class OrderListVo implements Serializable {

    @ApiModelProperty("订单ID")
    private String orderId;

    @ApiModelProperty("店铺类型")
    private String shopType;

    @ApiModelProperty("商品合计价格(考虑折扣优惠)")
    private double lastPrice;

    @ApiModelProperty("是否已完成支付")
    private String ifPayed;

    @ApiModelProperty("商品列表集合")
    private List<OrderPayedListVo> orderPayedListVos;
    
}
