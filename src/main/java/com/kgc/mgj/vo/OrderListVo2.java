package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 订单列表VO
 * @author jialin
 * @createTime 2020-09-21
 */
@Data
@ApiModel("订单列表VO")
public class OrderListVo2 implements Serializable {

    @ApiModelProperty("订单ID")
    private String orderId;

    @ApiModelProperty("店铺类型")
    private String shopType;

    @ApiModelProperty("商品主图")
    private String picMain;

    @ApiModelProperty("商品名称")
    private String itemName;

    @ApiModelProperty("商品价格")
    private double itemPrice;

    @ApiModelProperty("商品数量")
    private Integer itemCount;

    @ApiModelProperty( "商品合计价格(考虑折扣优惠)")
    private double lastPrice;

    @ApiModelProperty("是否已完成支付")
    private String ifPayed;

    @ApiModelProperty("收货地址")
    private String address;
}
