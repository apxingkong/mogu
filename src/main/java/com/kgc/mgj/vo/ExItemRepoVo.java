package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table ex_item_repo
 *
 * @mbg.generated do_not_delete_during_merge
 */
@Data
@ApiModel("商品库存表")
public class ExItemRepoVo {
    /**
     * Database Column Remarks:
     *   库存id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ex_item_repo.id
     *
     * @mbg.generated
     */
    @ApiModelProperty("库存id")
    private Integer id;

    /**
     * Database Column Remarks:
     *   商品编号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ex_item_repo.item_id
     *
     * @mbg.generated
     */
    @ApiModelProperty("商品编号")
    private String itemId;

    /**
     * Database Column Remarks:
     *   商品存量
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ex_item_repo.count
     *
     * @mbg.generated
     */
    @ApiModelProperty("商品存量")
    private Integer count;

}