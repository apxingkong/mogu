package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author jialin
 * @create 2020-10-07
 */
@Data
@ApiModel("收藏列表展示VO")
public class CollectionVo implements Serializable {

    @ApiModelProperty("店铺id")
    private Integer shopType;

    @ApiModelProperty("店铺名称")
    private String shopTypename;

    @ApiModelProperty("店铺logo")
    private String shopPic;

    @ApiModelProperty("上新商品个数")
    private Integer newItemCounts;

    @ApiModelProperty("最新上架时间")
    private String newItemTime;

    @ApiModelProperty("上架的商品列表")
    private List<CollectionItemsVo> collectionItemsVos;

}
