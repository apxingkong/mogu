package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 19:44
 */
@Data
@ApiModel("购物车页面商品的前端模型")
public class ItemCartVo implements Serializable {
    @ApiModelProperty("商品id")
    private String itemId;
    @ApiModelProperty("商品名称")
    private String itemName;
    @ApiModelProperty("缩略图路径")
    private String picMain;
    @ApiModelProperty("商品价格")
    private double itemPrice;
    @ApiModelProperty("当前商品加入购物车的数量")
    private Integer itemCount;
    @ApiModelProperty("商品打折后的价格")
    private double discountPrice;
    @ApiModelProperty("商品详情")
    private String itemDetail;
    @ApiModelProperty("商品所属的店铺信息")
    private String shopTypeName;
    @ApiModelProperty("使用的优惠券信息")
    private CartCouponVo cartCouponVo;
    @ApiModelProperty("当前商品的否库存")
    private int stock;

}
