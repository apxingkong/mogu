package com.kgc.mgj.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author jialin
 * @create 2020-10-08
 */
@Data
@ApiModel("足迹VO")
public class TraceVo implements Serializable {

    @ApiModelProperty("用户编号")
    private String userId;

    @ApiModelProperty("足迹中的商品个数")
    private Integer traceCounts;

    @ApiModelProperty("足迹的商品列表")
    private List<CollectionItemsVo> collectionItemsVos;
}
