package com.kgc.mgj.config.wxconfig;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author jialin
 * @createTime 2020-09-28
 */
@Data
@Component
@ConfigurationProperties(prefix = "wx.pay")
public class WxPayConfig {
    private String appid;
    private String mchid;
    private String notifyUrl;
    private String key;
    private String unifiedUrl;
}
