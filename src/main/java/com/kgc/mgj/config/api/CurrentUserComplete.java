package com.kgc.mgj.config.api;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class CurrentUserComplete implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        /*
         * 判断参数有没有添加 @CurrentUser注解
         * 判断该参数是否为UserVo
         */
        boolean flag = parameter.hasParameterAnnotation(CurrentUser.class) && parameter.getParameterType().isAssignableFrom(String.class);
        return flag;
    }
    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        /*
         * 从 nativeWebRequest获取 LoginRequiredComplete中添加到 request的 userVo
         * nativeWebRequest是增强版的request 第二个参数指定了作用域为request（还可以可以为 session）
         */
//        WxUserVo loginUserVo = (WxUserVo) nativeWebRequest.getAttribute("WxUserVo", RequestAttributes.SCOPE_REQUEST);
//        if (!ObjectUtils.isEmpty(loginUserVo))
//            return loginUserVo;
        return (String) nativeWebRequest.getAttribute("openid", RequestAttributes.SCOPE_REQUEST);
    }
}
