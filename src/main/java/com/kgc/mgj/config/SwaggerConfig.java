package com.kgc.mgj.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * http://localhost:18082/swagger-ui.html
 * http://localhost:18082/doc.html
 * @author jialin
 */
@Configuration
@EnableSwagger2 //开启Swagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfig {
    //配置了Swagger的Docket的bean实例
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.kgc.mgj.controller"))
                .build();
    }
    //配置Swagger信息apiInfo
    private ApiInfo apiInfo(){
        //作者信息
        Contact contact = new Contact("3组", "https://space.bilibili.com/102777715", "2079845319@qq.com");

        return new ApiInfo("3组的Swagger文档",
                "课工场KH83", "1.0",
                "https://space.bilibili.com/102777715",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
}