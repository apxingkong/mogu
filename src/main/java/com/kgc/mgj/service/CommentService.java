package com.kgc.mgj.service;

import com.kgc.mgj.dto.Comment;
import com.kgc.mgj.dto.CommentDto;
import com.kgc.mgj.dto.CommentExample;
import com.kgc.mgj.mapper.CommentMapper;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.vo.CommentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XuHui on 2020/9/22
 */
@Service
public class CommentService {
    @Autowired(required = false)
    private CommentMapper commentMapper;
    @Autowired
    private ExUserService exUserService;

    /**
     * 查询评论详情(首页默认一条)
     *
     * @param pageNo
     * @param pageSize
     * @param itemId
     * @return
     */
    public PageUtils<CommentVo> getComment(Integer pageNo, Integer pageSize, String itemId) {
        if (pageNo == null){
            pageNo = 1;
        }
        if (pageSize == null){
            pageSize = 4;
        }
        
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);

        CommentExample commentExample = new CommentExample();
        long commentCount = commentMapper.countByExample(commentExample);
        List<Comment> commentDtos = commentMapper.queryItemsComments(itemId,pageUtils.getPageNo(),pageSize);

        List<CommentVo> commentVos = new ArrayList<CommentVo>();
        commentDtos.forEach(commentDto -> {
            CommentVo commentVo = new CommentVo();
            BeanUtils.copyProperties(commentDto, commentVo);
            commentVos.add(commentVo);
        });
        pageUtils.setTotalCount(commentCount);
        pageUtils.setCurrentList(commentVos);
        return pageUtils;

    }

}
