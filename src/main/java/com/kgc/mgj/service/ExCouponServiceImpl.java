package com.kgc.mgj.service;

import com.kgc.mgj.dto.*;
import com.kgc.mgj.mapper.ExCartdetailMapper;
import com.kgc.mgj.mapper.ExCouponMapper;
import com.kgc.mgj.mapper.ExItemcouponMapper;
import com.kgc.mgj.mapper.ExItemsMapper;
import com.kgc.mgj.params.CouponBestLinkedList;
import com.kgc.mgj.params.CouponBestNode;
import com.kgc.mgj.utils.DateUtils;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.utils.UuidUtils;
import com.kgc.mgj.vo.ExCouponVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author jialin
 * @createTime 2020-09-03
 */
@Log4j
@Service
public class ExCouponServiceImpl implements ExCouponService {

    @Autowired
    private ExCouponMapper exCouponMapper;

    @Autowired
    private ExItemsMapper exItemsMapper;

    @Autowired
    private ExItemcouponMapper exItemcouponMapper;

    /**
     * 获取最优优惠券，返回最优优惠券编号
     * @param itemId
     */
    public String chooseBestCoupon(String itemId, Integer itemCount){
        //优惠券单链表，管理最优优惠券
        CouponBestLinkedList couponBestLinkedList = new CouponBestLinkedList();
        //查询商品所有的优惠券
        List<ExCouponVo> exCouponsVo = queryCoupon(itemId);
        //取得最小的优惠后价格
        exCouponsVo.forEach(exCouponVo -> {
            //此优惠券的满多少金额
            double withAmount = exCouponVo.getWithAmount();
            //此优惠券的减多少金额
            double useAmount = exCouponVo.getUsedAmount();
            ExItems exItems = exItemsMapper.selectByPrimaryKey(itemId);
            //商品原价
            double itemPrice = null == exItems.getDiscountPrice() ? exItems.getItemPrice() : exItems.getDiscountPrice();
            double itemPriceTotal = itemPrice * itemCount;
            //算出此优惠券使用后的价格
            itemPriceTotal = itemPriceTotal > withAmount ? itemPriceTotal - useAmount : itemPriceTotal;
            CouponBestNode couponBestNode = new CouponBestNode(itemPriceTotal, exCouponVo.getCouponId());
            //加入链表
            couponBestLinkedList.addBestCoupon(couponBestNode);
        });
        //拿出最优优惠券，链表的第一个
        return couponBestLinkedList.getFirst();
    }

    /**
     * 添加优惠券
     *
     * @param exCoupon
     * @return
     */
    public boolean addExCoupon(ExCoupon exCoupon) {
        try {
            //模拟前端选择时间，有效开始时间和结束时间
            String startTimeStr = "2020-10-10 10:12:42";
            //设置优惠券编号
            exCoupon.setCouponId(UuidUtils.getTimeUUID());
            exCoupon.setStartTime(DateUtils.formatStringToDate(startTimeStr, DateUtils.DATE_FORMAT_FULL));
            exCouponMapper.insertSelective(exCoupon);
            return true;
        } catch (Exception e) {
            log.error("添加优惠券错误！" + e);
        }
        return false;
    }

    /**
     * 删除优惠券
     *
     * @param ids
     * @return
     */
    public boolean deleteExCoupon(String... ids) {
        int row = 0;
        for (String id : ids) {
            row = exCouponMapper.deleteByPrimaryKey(id);
        }
        return row > 0;
    }

    /**
     * 修改优惠券
     *
     * @param exCoupon
     * @return
     */
    public boolean modifyExCoupon(ExCoupon exCoupon) {
        int row = 0;
        try {
            //模拟前端选择时间，有效开始时间和结束时间
            String startTimeStr = "2020-10-21 10:12:42";
            String endTimeStr = "2021-10-21 10:12:42";
            exCoupon.setStartTime(DateUtils.formatStringToDate(startTimeStr, DateUtils.DATE_FORMAT_FULL));
            //设置修改时间
            exCoupon.setUpdateTime(new Date(System.currentTimeMillis()));
            row = exCouponMapper.updateByPrimaryKeySelective(exCoupon);
        } catch (Exception e) {
            log.error("修改优惠券错误！" + e);
        }
        return row > 0;
    }

    /**
     * 查询优惠券，分页，多条件
     *
     * @param exCoupon
     * @param pageNo
     * @param pageSize
     * @return
     */
    public PageUtils<ExCouponVo> queryExCoupon(ExCoupon exCoupon, int pageNo, int pageSize) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        //查询所有条数
        long totalCouponCount = exCouponMapper.queryExCoupon(exCoupon);
        pageUtils.setTotalCount(totalCouponCount);
        //分页查询
        List<ExCoupon> exCouponList = exCouponMapper.queryExCoupons(exCoupon, pageUtils.getPageNo(), pageSize);
        //复制Vo
        List<ExCouponVo> exCouponVoList = new ArrayList<ExCouponVo>();
        exCouponList.forEach(exCoupon1 -> {
            ExCouponVo exCouponVo = new ExCouponVo();
            BeanUtils.copyProperties(exCoupon1, exCouponVo);
            exCouponVoList.add(exCouponVo);
        });
        pageUtils.setCurrentList(exCouponVoList);
        return pageUtils;
    }

    /**
     * 根据商品编号查询优惠券信息
     * @param itemId
     * @return
     */
    @Override
    public List<ExCouponVo> queryCoupon(String itemId) {
        List<ExCouponVo> exCouponVos = new ArrayList<>();
        //根据商品优惠券对应表查询
        ExItemcouponExample exItemcouponExample = new ExItemcouponExample();
        exItemcouponExample.createCriteria().andItemIdEqualTo(itemId);
        List<ExItemcoupon> exItemcoupons = exItemcouponMapper.selectByExample(exItemcouponExample);
        exItemcoupons.forEach(exItemcoupon -> {
            ExCouponVo exCouponVo = new ExCouponVo();
            //商品名称
            exCouponVo.setItemName(exItemsMapper.selectByPrimaryKey(itemId).getItemName());
            ExCoupon exCoupon = exCouponMapper.selectByPrimaryKey(exItemcoupon.getCouponId());
            BeanUtils.copyProperties(exCoupon, exCouponVo);
            //时间格式
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
            exCouponVo.setStartTime(simpleDateFormat.format(exCoupon.getStartTime()));
            //判断有效期
            //当前时间
            Long currTime = System.currentTimeMillis();
            //有效期截至时间
            long couponTime = exCoupon.getStartTime().getTime() + 86400000 * exCoupon.getValueTime();
            //在时间截至之前
            if (currTime < couponTime){
                exCouponVos.add(exCouponVo);
            }
        });
        return exCouponVos;
    }

}
