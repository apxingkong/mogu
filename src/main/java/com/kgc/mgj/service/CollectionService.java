package com.kgc.mgj.service;

import com.kgc.mgj.dto.*;
import com.kgc.mgj.mapper.ExItemsMapper;
import com.kgc.mgj.mapper.ShopInfoMapper;
import com.kgc.mgj.mapper.UserCollectionMapper;
import com.kgc.mgj.vo.CollectionItemsVo;
import com.kgc.mgj.vo.CollectionVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jialin
 * @create 2020-10-03
 */
@Service
@Log4j
public class CollectionService {

    @Autowired
    private UserCollectionMapper userCollectionMapper;

    @Autowired
    private ShopInfoMapper shopInfoMapper;

    @Autowired
    private ExItemsMapper exItemsMapper;

    /**
     * 添加收藏
     *
     * @param shopId
     * @param userId
     */
    public void addCollection(Integer shopId, String userId) {
        UserCollection userCollection = new UserCollection();
        userCollection.setShopId(shopId);
        userCollection.setUserId(userId);
        userCollectionMapper.insertSelective(userCollection);
    }

    /**
     * 获取收藏列表
     *
     * @param userId
     * @return
     */
    public List<CollectionVo> getCollection(String userId) {
        List<CollectionVo> collectionVos = new ArrayList<CollectionVo>();
        //根据userID获取收藏的店铺
        UserCollectionExample userCollectionExample = new UserCollectionExample();
        userCollectionExample.createCriteria().andUserIdEqualTo(userId);
        List<UserCollection> userCollections = userCollectionMapper.selectByExample(userCollectionExample);
        //遍历查找到的所有店铺
        for (UserCollection userCollection : userCollections) {
            List<CollectionItemsVo> collectionItemsVos = new ArrayList<CollectionItemsVo>();
            Integer newItemCounts = null;
            //查询更新时间最新的十件商品
            List<ExItems> exItems = exItemsMapper.queryItemsOrderByTime(userCollection.getShopId(), 0, 10);
            //创建收藏页面商品VO
            exItems.forEach(exItems1 -> {
                CollectionItemsVo collectionItemsVo = new CollectionItemsVo();
                BeanUtils.copyProperties(exItems1, collectionItemsVo);
                //商品价格
                //如果没有折扣价，就是原价
                if (null == exItems1.getDiscountPrice()){
                    collectionItemsVo.setItemPrice(exItems1.getItemPrice());
                }else {
                    collectionItemsVo.setItemPrice(exItems1.getDiscountPrice());
                }
                //创建时间
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
                collectionItemsVo.setItemCreateTime(simpleDateFormat.format(exItems1.getCreateTime()));
                collectionItemsVos.add(collectionItemsVo);
            });
            CollectionVo collectionVo = new CollectionVo();
            //根据店铺编号查询店铺信息
            ShopInfoExample shopInfoExample = new ShopInfoExample();
            shopInfoExample.createCriteria().andShopTypeEqualTo(userCollection.getShopId());
            ShopInfo shopInfo = shopInfoMapper.selectByExample(shopInfoExample).get(0);
            //复制VO，店铺id，店铺名称，店铺logo
            BeanUtils.copyProperties(shopInfo, collectionVo);
            newItemCounts = collectionItemsVos.size();
            //新上架的商品数量
            collectionVo.setNewItemCounts(newItemCounts);
            //最新上架时间
            collectionVo.setNewItemTime(collectionItemsVos.get(0).getItemCreateTime());
            //上架的商品列表
            collectionVo.setCollectionItemsVos(collectionItemsVos);
            collectionVos.add(collectionVo);
        }
        return collectionVos;
    }

}
