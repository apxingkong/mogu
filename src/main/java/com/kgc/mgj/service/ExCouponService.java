package com.kgc.mgj.service;

import com.kgc.mgj.dto.ExCoupon;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.vo.ExCouponVo;

import java.util.List;

/**
 * @author jialin
 * @createTime 2020-09-16
 */
public interface ExCouponService {
    boolean addExCoupon(ExCoupon exCoupon);
    boolean deleteExCoupon(String... ids);
    boolean modifyExCoupon(ExCoupon exCoupon);
    PageUtils<ExCouponVo> queryExCoupon(ExCoupon exCoupon, int pageNo, int pageSize);
    List<ExCouponVo> queryCoupon(String itemId);
    String chooseBestCoupon(String itemId, Integer itemCount);
}
