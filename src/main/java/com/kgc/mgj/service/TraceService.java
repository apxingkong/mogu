package com.kgc.mgj.service;

import com.kgc.mgj.dto.*;
import com.kgc.mgj.mapper.ExItemsMapper;
import com.kgc.mgj.mapper.ExItemtypeMapper;
import com.kgc.mgj.mapper.TraceMapper;
import com.kgc.mgj.vo.CollectionItemsVo;
import com.kgc.mgj.vo.TraceVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author jialin
 * @create 2020-10-08
 */
@Service
public class TraceService {

    @Autowired
    private TraceMapper traceMapper;

    @Autowired
    private ExItemsMapper exItemsMapper;

    @Autowired
    private ExItemtypeMapper exItemtypeMapper;

    /**
     * 添加足迹
     * @param itemId
     * @param itemType
     * @param userId
     */
    public void addTrace(String itemId, Integer itemType, String userId){
        //TraceExample traceExample = new TraceExample();
        //traceExample.createCriteria().andItemIdEqualTo(itemId).andItemTypeEqualTo(itemType).andUserIdEqualTo(userId);
        Trace trace = new Trace();
        trace.setItemId(itemId);
        trace.setItemType(itemType);
        trace.setUserId(userId);
        trace.setReadTime(new Date(System.currentTimeMillis()));

        ExItems exItems = exItemsMapper.selectByPrimaryKey(itemId);
        //商品类型浏览次数加一
        ExItemtypeExample exItemtypeExample = new ExItemtypeExample();
        exItemtypeExample.createCriteria().andItemTypeEqualTo(exItems.getItemType());
        ExItemtype exItemtype = exItemtypeMapper.selectByExample(exItemtypeExample).get(0);
        exItemtype.setReadcounts(exItemtype.getReadcounts() + 1);
        exItemtypeMapper.updateByPrimaryKey(exItemtype);
        //商品类型权重更新
        List<ExItems> exItems1 = new ArrayList<>();
        ExItemsExample exItemsExample = new ExItemsExample();
        exItemsExample.createCriteria().andItemTypeEqualTo(exItemtype.getItemType());
        exItems1 = exItemsMapper.selectByExample(exItemsExample);
        exItems1.forEach(exItems2 -> {
            exItems2.setReadcounts(exItemtype.getReadcounts());
            exItemsMapper.updateByPrimaryKey(exItems2);
        });
        traceMapper.insertSelective(trace);
    }

    /**
     * 查询用户的足迹
     * @param userId
     * @return
     */
    public List<TraceVo> queryTraces(String userId){
        List<TraceVo> traceVos = new ArrayList<TraceVo>();
        //根据userId获取足迹的商品类型
        TraceExample traceExample = new TraceExample();
        traceExample.createCriteria().andUserIdEqualTo(userId);
        List<Trace> traces = traceMapper.selectByExample(traceExample);
        Set<Integer> itemsTypes = new HashSet<>();
        traces.forEach(trace -> {
            itemsTypes.add(trace.getItemType());
        });
        //根据商品类型查询足迹中的对应分类的商品
        for (Integer itemsType : itemsTypes) {
            List<CollectionItemsVo> collectionItemsVos = new ArrayList<CollectionItemsVo>();
            Integer traceItemCounts = null;
            //查询足迹中的对应分类的商品
            TraceExample traceExample1 = new TraceExample();
            traceExample1.setOrderByClause("read_time DESC");
            traceExample1.createCriteria().andItemTypeEqualTo(itemsType);
            List<Trace> traces1 = traceMapper.selectByExample(traceExample1);
            for (Trace trace : traces1) {
                ExItems exItems = new ExItems();
                CollectionItemsVo collectionItemsVo = new CollectionItemsVo();
                exItems = exItemsMapper.selectByPrimaryKey(trace.getItemId());
                BeanUtils.copyProperties(exItems, collectionItemsVo);
                //商品价格
                //如果没有折扣价，就是原价
                if (null == exItems.getDiscountPrice()){
                    collectionItemsVo.setItemPrice(exItems.getItemPrice());
                }else {
                    collectionItemsVo.setItemPrice(exItems.getDiscountPrice());
                }
                //创建时间
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
                collectionItemsVo.setItemCreateTime(simpleDateFormat.format(exItems.getCreateTime()));
                collectionItemsVos.add(collectionItemsVo);
            }
            TraceVo traceVo = new TraceVo();
            //用户编号
            traceVo.setUserId(userId);
            //足迹中的商品个数
            traceItemCounts = collectionItemsVos.size();
            traceVo.setTraceCounts(traceItemCounts);
            //足迹的商品列表
            traceVo.setCollectionItemsVos(collectionItemsVos);
            traceVos.add(traceVo);
        }
        return traceVos;
    }
}
