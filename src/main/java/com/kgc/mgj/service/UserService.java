package com.kgc.mgj.service;

import com.kgc.mgj.dto.*;
import com.kgc.mgj.mapper.ExOrderMapper;
import com.kgc.mgj.mapper.ExUserMapper;
import com.kgc.mgj.mapper.ExUsercouponMapper;
import com.kgc.mgj.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jialin
 * @create 2020-10-09
 */
@Service
public class UserService {

    @Autowired(required = false)
    private ExUserMapper exUserMapper;

    @Autowired(required = false)
    private ExOrderMapper exOrderMapper;

    @Autowired(required = false)
    private ExUsercouponMapper exUsercouponMapper;

    public UserVo queryUser(String userId){
        ExUser exUser = exUserMapper.selectByPrimaryKey(userId);
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(exUser,userVo);
        //待支付数
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andUserIdEqualTo(userId);
        List<ExOrder> exOrders = exOrderMapper.selectByExample(exOrderExample);
        int count = exOrders.size();
        userVo.setToPayCount(count);
        //优惠券数
        ExUsercouponExample exUsercouponExample = new ExUsercouponExample();
        exUsercouponExample.createCriteria().andUserIdEqualTo(userId);
        List<ExUsercoupon> exUsercoupons = exUsercouponMapper.selectByExample(exUsercouponExample);
        int cCount = exUsercoupons.size();
        userVo.setCouponCount(cCount);

        return userVo;
    }

}
