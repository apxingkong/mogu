package com.kgc.mgj.service;

import com.kgc.mgj.dto.*;
import com.kgc.mgj.mapper.ExItemsMapper;
import com.kgc.mgj.mapper.ExItemtypeMapper;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.vo.ExItemsVo;
import com.kgc.mgj.vo.ExItemtypeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExItemsListService {
    @Autowired(required = false)
    private ExItemsMapper exItemsMapper;

    @Autowired(required = false)
    private ExItemtypeMapper exItemtypeMapper;

    public List<ExItemtypeVo> selectType(Integer Id) {
        List<ExItemtypeVo> exItemtypeVos = new ArrayList<>();
        ExItemtypeExample exItemtypeExample = new ExItemtypeExample();
        exItemtypeExample.createCriteria().andIdIsNotNull();

        List<ExItemtype> exItemtypeDtos = exItemtypeMapper.selectByExample(exItemtypeExample);
        for (ExItemtype exItemtypeDto : exItemtypeDtos) {
            ExItemtypeVo exItemtypeVo = new ExItemtypeVo();
            BeanUtils.copyProperties(exItemtypeDto, exItemtypeVo);
            exItemtypeVos.add(exItemtypeVo);
        }

        return exItemtypeVos;
    }

    public PageUtils<ExItemsVo> select(String itemName, Integer itemType, Integer pageNo, Integer pageSize) {
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }

        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);

        ExItemsExample exItemsExample = new ExItemsExample();
        long exItemsCount = exItemsMapper.countByExample(exItemsExample);

        exItemsExample.setLimit(pageUtils.getPageNo());
        exItemsExample.setOffset(pageSize);
        if (itemType != null) {
            exItemsExample.createCriteria().andItemTypeEqualTo(itemType);
        } else if (itemName != null) {
            exItemsExample.createCriteria().andItemNameLike("%" + itemName + "%");
        } else {
            exItemsExample.createCriteria().andItemNameIsNotNull().andItemTypeIsNotNull();
        }

        exItemsExample.setOrderByClause("readCounts desc");
        List<ExItems> exItemsDtos = exItemsMapper.selectByExample(exItemsExample);

        List<ExItemsVo> exItemsVos = new ArrayList<>();
        for (ExItems exItemsDto : exItemsDtos) {
            ExItemsVo exItemsVo = new ExItemsVo();
            BeanUtils.copyProperties(exItemsDto, exItemsVo);
            exItemsVos.add(exItemsVo);
        }

        pageUtils.setCurrentList(exItemsVos);
        pageUtils.setTotalCount(exItemsCount);

        return pageUtils;
    }
}