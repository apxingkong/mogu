package com.kgc.mgj.service;

import com.kgc.mgj.dto.ExItems;
import com.kgc.mgj.mapper.ExItemsMapper;
import com.kgc.mgj.vo.ExItemsVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by XuHui on 2020/9/21
 */
@Service
public class ItemService {
    @Autowired
    private ExItemsMapper exItemsMapper;
    
    @Autowired
    private TraceService traceService;

    /**
     * 查询商品详情
     *
     * @param itemId
     * @return
     */
    public ExItemsVo queryGoods(String itemId, String userId) {
        ExItems exItemsDto = exItemsMapper.selectByPrimaryKey(itemId);
        String picDetail = exItemsDto.getPicDetail();
        ExItemsVo exItemsVo = new ExItemsVo();
        BeanUtils.copyProperties(exItemsDto, exItemsVo);
        //足迹
        traceService.addTrace(itemId, exItemsDto.getItemType(), userId);
        return exItemsVo;
    }
    
}
