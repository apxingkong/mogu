package com.kgc.mgj.service;

import com.kgc.mgj.dto.ExUser;
import com.kgc.mgj.dto.ReceiverInfo;
import com.kgc.mgj.dto.ReceiverInfoExample;
import com.kgc.mgj.mapper.ExCouponMapper;
import com.kgc.mgj.mapper.ExUserMapper;
import com.kgc.mgj.mapper.ExUsercouponMapper;
import com.kgc.mgj.mapper.ReceiverInfoMapper;
import com.kgc.mgj.vo.ExUserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XuHui on 2020/9/22
 */
@Service
public class ExUserService {
    @Autowired(required = false)
    private ExUserMapper exUserMapper;
    @Autowired(required = false)
    private ExUsercouponMapper exUsercouponMapper;
    @Autowired(required = false)
    private ExCouponMapper exCouponMapper;
    @Autowired(required = false)
    private ReceiverInfoMapper receiverInfoMapper;

    /**
     * 查询当前用户信息
     *
     * @param UserId
     * @return
     */
    public ExUserVo getUser(String UserId) {
        ExUser exUserDto = exUserMapper.selectByPrimaryKey(UserId);
        ExUserVo exUserVo = new ExUserVo();
        BeanUtils.copyProperties(exUserDto, exUserVo);
        return exUserVo;
    }

    /**
     * 查询用户的收货地址
     * @param userId
     * @return
     */
    public List<String> queryAddress(String userId){
        List<String> address = new ArrayList<>();
        ReceiverInfoExample receiverInfoExample=new ReceiverInfoExample();
        receiverInfoExample.createCriteria().andUserIdEqualTo(userId);
        //查询用户
        ReceiverInfo receiverInfoDto=receiverInfoMapper.selectByExample(receiverInfoExample).get(0);
        String[] addresses = receiverInfoDto.getReceiverAddress().split(";");
        for (String s : addresses) {
            address.add(s);
        }
        return address;
    }

}
