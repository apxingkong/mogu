package com.kgc.mgj.service;

import com.kgc.mgj.dto.ExItemRepo;
import com.kgc.mgj.dto.ExItemRepoExample;
import com.kgc.mgj.mapper.ExItemRepoMapper;
import com.kgc.mgj.vo.ExItemRepoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by XuHui on 2020/9/22
 */
@Service
public class StockService {
    @Autowired
    private ExItemRepoMapper exItemRepoMapper;

    //查询商品库存详情
    public ExItemRepoVo getStock(String itemId) {
        ExItemRepoExample exItemRepoExample = new ExItemRepoExample();
        exItemRepoExample.createCriteria().andItemIdEqualTo(itemId);
        ExItemRepo exItemRepoDto = exItemRepoMapper.selectByExample(exItemRepoExample).get(0);
        ExItemRepoVo exItemRepoVo = new ExItemRepoVo();
        BeanUtils.copyProperties(exItemRepoDto, exItemRepoVo);
        return exItemRepoVo;
    }
}
