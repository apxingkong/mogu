package com.kgc.mgj.service;

import com.kgc.mgj.dto.*;
import com.kgc.mgj.mapper.*;
import com.kgc.mgj.params.QueryOrderParam;
import com.kgc.mgj.utils.PageUtils;
import com.kgc.mgj.utils.UuidUtils;
import com.kgc.mgj.vo.OrderPayedListVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author jialin
 * @create 2020-10-08
 */
@Log4j
@Service
public class OrderService {

    @Autowired
    private ExOrderMapper exOrderMapper;

    @Autowired
    private ExOrderdetailMapper exOrderdetailMapper;

    @Autowired
    private ExCartdetailMapper exCartdetailMapper;

    @Autowired
    private ExItemsMapper exItemsMapper;

    @Autowired
    private ExUserMapper exUserMapper;

    @Autowired
    private ShopInfoMapper shopInfoMapper;

    @Autowired
    private ExItemtypeMapper exItemtypeMapper;

    @Autowired
    private ExCouponMapper exCouponMapper;

    @Autowired
    private ExItemRepoMapper exItemRepoMapper;

    @Autowired
    private StreamcouponMapper streamcouponMapper;

    @Autowired
    private ExUsercouponMapper exUsercouponMapper;

    /**
     * 生成未支付订单
     *
     * @param address
     * @param userId
     * @param itemIds
     */
    public Integer createOrder(String address, String userId, String... itemIds) {
        //生成统一共用订单ID
        String orderId = UuidUtils.getTimeUUID();
        double totalPrice = 0;
        for (String itemId : itemIds) {
            ExCartdetail exCartdetailDto = exCartdetailMapper.selectByItemId(itemId, userId);
            ExOrderdetail exOrderdetailDto = new ExOrderdetail();
            BeanUtils.copyProperties(exCartdetailDto, exOrderdetailDto);
            userId = exOrderdetailDto.getUserId();

            //查看库存
            ExItemRepoExample exItemRepoExample = new ExItemRepoExample();
            exItemRepoExample.createCriteria().andItemIdEqualTo(itemId);
            ExItemRepo exItemRepos = exItemRepoMapper.selectByExample(exItemRepoExample).get(0);
            Integer itemCounts = exItemRepos.getCount();
            if (itemCounts < exCartdetailDto.getItemCount()) {
                //库存不足
                return 301;
            }

            //商品价格
            //如果没有折扣价，就是原价
            double price;
            if (null == exItemsMapper.selectByPrimaryKey(itemId).getDiscountPrice()) {
                price = exItemsMapper.selectByPrimaryKey(itemId).getItemPrice();
            } else {
                price = exItemsMapper.selectByPrimaryKey(itemId).getDiscountPrice();
            }
            //double price = itemService.queryGoods(itemId).getItemPrice();
            ExItems exItems = exItemsMapper.selectByPrimaryKey(itemId);
            int itemCount = exOrderdetailDto.getItemCount();
            double itemTotalPrice = price * itemCount;
            //优惠券判断
            if (null != exCartdetailDto.getCouponId()) {
                ExCoupon exCoupon = exCouponMapper.selectByPrimaryKey(exCartdetailDto.getCouponId());
                //满多少金额
                double withAmount = exCoupon.getWithAmount();
                //减多少金额
                double useAmount = exCoupon.getUsedAmount();
                itemTotalPrice = itemTotalPrice > withAmount ? itemTotalPrice - useAmount : itemTotalPrice;
            }
            totalPrice += itemTotalPrice;
            exOrderdetailDto.setOrderId(orderId);
            exOrderdetailDto.setItemPrice(price);
            //价格保留两位小数
            double formatPrice = 0.0;
            formatPrice = (double) Math.round(itemTotalPrice * 100) / 100;

            //判断用户余额
            ExUser user = exUserMapper.selectByPrimaryKey(userId);
            double userMoney = user.getUserBalance();
            if (userMoney < formatPrice) {
                //余额不足
                return 302;
            }

            exOrderdetailDto.setItemTotalprice(formatPrice);
            exOrderdetailDto.setIfPay(0);
            exOrderdetailDto.setAddress(address);
            exOrderdetailDto.setShopId(exItems.getShopId());
            exOrderdetailDto.setItemType(exItems.getItemType());
            exOrderdetailMapper.insertSelective(exOrderdetailDto);
            //删除购物车中itemId对应信息
            exCartdetailMapper.deleteByItemId(itemId);
        }
        ExOrder exOrderDto = new ExOrder();
        exOrderDto.setOrderId(orderId);
        exOrderDto.setUserId(userId);
        //价格保留两位小数
        double formatPrice = 0.0;
        formatPrice = (double) Math.round(totalPrice * 100) / 100;
        exOrderDto.setTotalPrice(formatPrice);
        exOrderDto.setOrderType(0);
        exOrderDto.setOrderOvertime(0);
        exOrderDto.setOrderIfdel(0);
        exOrderDto.setOrderIfreceive(0);
        exOrderMapper.insertSelective(exOrderDto);
        return 200;
    }

    /**
     * 生成已支付订单
     * 1.查看库存
     * 2.计算商品价格
     * 3.判断优惠券
     * 4.判断用户余额
     * 5.插入订单详情表
     * 6.删除购物车表的信息
     * 7.插入流水表
     * 8.添加已支付订单
     * 9.用户扣款
     * 10.生成流水表
     * 11.减库存
     * 12.更改用户优惠券对应表的使用状态
     *
     * @param address
     * @param userId
     * @param itemIds
     */
    public Integer createOrderPayed(String address, String userId, String... itemIds) {
        //生成统一共用订单ID
        String orderId = UuidUtils.getTimeUUID();
        double totalPrice = 0;
        for (String itemId : itemIds) {
            ExCartdetail exCartdetailDto = exCartdetailMapper.selectByItemId(itemId, userId);
            ExOrderdetail exOrderdetailDto = new ExOrderdetail();
            BeanUtils.copyProperties(exCartdetailDto, exOrderdetailDto);
            userId = exOrderdetailDto.getUserId();

            //查看库存
            ExItemRepoExample exItemRepoExample = new ExItemRepoExample();
            exItemRepoExample.createCriteria().andItemIdEqualTo(itemId);
            ExItemRepo exItemRepos = exItemRepoMapper.selectByExample(exItemRepoExample).get(0);
            Integer itemCounts = exItemRepos.getCount();
            if (itemCounts < exCartdetailDto.getItemCount()) {
                //库存不足
                return 301;
            }

            //商品价格
            //如果没有折扣价，就是原价
            double price;
            if (null == exItemsMapper.selectByPrimaryKey(itemId).getDiscountPrice()) {
                price = exItemsMapper.selectByPrimaryKey(itemId).getItemPrice();
            } else {
                price = exItemsMapper.selectByPrimaryKey(itemId).getDiscountPrice();
            }
            //double price = itemService.queryGoods(itemId).getItemPrice();
            ExItems exItems = exItemsMapper.selectByPrimaryKey(itemId);
            int itemCount = exOrderdetailDto.getItemCount();
            double itemTotalPrice = price * itemCount;
            //优惠券判断
            if (null != exCartdetailDto.getCouponId()) {
                //判断优惠券是否失效
                ExCoupon exCoupon = exCouponMapper.selectByPrimaryKey(exCartdetailDto.getCouponId());
                //当前时间
                Long currTime = System.currentTimeMillis();
                //有效期截至时间
                long couponTime = exCoupon.getStartTime().getTime() + 86400000 * exCoupon.getValueTime();
                if (currTime < couponTime) {
                    //有效
                    //满多少金额
                    double withAmount = exCoupon.getWithAmount();
                    //减多少金额
                    double useAmount = exCoupon.getUsedAmount();
                    itemTotalPrice = itemTotalPrice > withAmount ? itemTotalPrice - useAmount : itemTotalPrice;
                }
            }
            totalPrice += itemTotalPrice;
            exOrderdetailDto.setOrderId(orderId);
            exOrderdetailDto.setItemPrice(price);
            //价格保留两位小数
            double formatPrice = 0.0;
            formatPrice = (double) Math.round(itemTotalPrice * 100) / 100;

            //判断用户余额
            ExUser user = exUserMapper.selectByPrimaryKey(userId);
            double userMoney = user.getUserBalance();
            if (userMoney < formatPrice) {
                //余额不足
                return 302;
            }

            exOrderdetailDto.setItemTotalprice(formatPrice);
            exOrderdetailDto.setIfPay(1);
            exOrderdetailDto.setAddress(address);
            exOrderdetailDto.setShopId(exItems.getShopId());
            exOrderdetailDto.setItemType(exItems.getItemType());
            exOrderdetailMapper.insertSelective(exOrderdetailDto);
            //删除购物车中itemId对应信息
            exCartdetailMapper.deleteByItemId(itemId);

            //生成流水表
            Streamcoupon streamcoupon = new Streamcoupon();
            streamcoupon.setCouponId(exCartdetailDto.getCouponId());
            streamcoupon.setItemId(itemId);
            streamcoupon.setUserId(userId);
            streamcoupon.setShopId(Integer.parseInt(exItems.getShopId()));
            streamcouponMapper.insertSelective(streamcoupon);

            //减库存
            exItemRepos.setCount(itemCounts - exCartdetailDto.getItemCount());
            exItemRepos.setUpdateTime(new Date(System.currentTimeMillis()));
            exItemRepoMapper.updateByPrimaryKey(exItemRepos);

            //更改用户优惠券对应表
            ExUsercoupon exUsercoupon;
            ExUsercouponExample exUsercouponExample = new ExUsercouponExample();
            exUsercouponExample.createCriteria().andUserIdEqualTo(userId).andIfUseEqualTo(0).andCouponIdEqualTo(exCartdetailDto.getCouponId());
            exUsercoupon = exUsercouponMapper.selectByExample(exUsercouponExample).get(0);
            exUsercoupon.setIfUse(1);
            exUsercouponMapper.updateByPrimaryKey(exUsercoupon);
        }
        ExOrder exOrderDto = new ExOrder();
        exOrderDto.setOrderId(orderId);
        exOrderDto.setUserId(userId);
        //价格保留两位小数
        double formatPrice = 0.0;
        formatPrice = (double) Math.round(totalPrice * 100) / 100;
        exOrderDto.setTotalPrice(formatPrice);
        exOrderDto.setOrderType(1);
        exOrderDto.setOrderOvertime(0);
        exOrderDto.setOrderIfdel(0);
        exOrderDto.setOrderIfreceive(0);
        exOrderMapper.insertSelective(exOrderDto);

        //用户扣款
        ExUser exUser = exUserMapper.selectByPrimaryKey(userId);
        //当前余额
        double userPrice = (double) Math.round((exUser.getUserBalance() - totalPrice) * 100) / 100;
        //用户信息更新
        exUser.setUserBalance(userPrice);
        exUserMapper.updateByPrimaryKeySelective(exUser);

        return 200;
    }

    /**
     * 根据条件查询订单
     *
     * @param userId
     * @return
     */
    public PageUtils<OrderPayedListVo> queryAllItemsPerOrder(QueryOrderParam queryOrderParam, String userId, Integer pageNo, Integer pageSize) {
        queryOrderParam.setUserId(userId);
        if (pageNo == null) {
            pageNo = 1;
        }
        if (pageSize == null) {
            pageSize = 4;
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        //商品主题为空
        if (null == queryOrderParam.getItemType()) {
            //查询总条数
            Long totalCount = exOrderMapper.queryAllItemsPerOrderCounts(queryOrderParam, pageUtils.getPageNo(), pageSize);
            pageUtils.setTotalCount(totalCount);
            //分页查询
            List<ExOrder> exOrders = exOrderMapper.queryAllItemsPerOrder(queryOrderParam, pageUtils.getPageNo(), pageSize);
            //创建订单列表VO集合对象
            List<OrderPayedListVo> orderPayedListVos = new ArrayList<>();
            for (ExOrder exOrder : exOrders) {
                //根据orderId查询所有购买的商品详情
                ExOrderdetailExample exOrderdetailExample = new ExOrderdetailExample();
                exOrderdetailExample.createCriteria().andOrderIdEqualTo(exOrder.getOrderId());
                List<ExOrderdetail> exOrderDetails = exOrderdetailMapper.selectByExample(exOrderdetailExample);
                exOrderDetails.forEach(exOrderDetail -> {
                    OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
                    BeanUtils.copyProperties(exOrderDetail, orderPayedListVo);
                    ExItems exItems = exItemsMapper.selectByPrimaryKey(exOrderDetail.getItemId());
                    //商品主图
                    orderPayedListVo.setPicMain(exItems.getPicMain());
                    //商品名称
                    orderPayedListVo.setItemName(exItems.getItemName());
                    //店铺名称
                    ShopInfoExample shopInfoExample = new ShopInfoExample();
                    shopInfoExample.createCriteria().andShopTypeEqualTo(Integer.valueOf(exOrderDetail.getShopId()));
                    orderPayedListVo.setShopTypeName(shopInfoMapper.selectByExample(shopInfoExample).get(0).getShopTypename());
                    //商品主题（0：上衣，1：裙装，2：短裤，3:女鞋...）
                    ExItemtypeExample exItemtypeExample = new ExItemtypeExample();
                    exItemtypeExample.createCriteria().andItemTypeEqualTo(exItems.getItemType());
                    orderPayedListVo.setItemType(exItemtypeMapper.selectByExample(exItemtypeExample).get(0).getTypeName());
                    //是否超时(0：未超时，1：已超时)
                    orderPayedListVo.setOrderOvertime(exOrder.getOrderOvertime() == 0 ? "未超时" : "已超时");
                    //是否删除(0：未删除，1：已删除)假删除
                    orderPayedListVo.setOrderIfdel(exOrder.getOrderIfdel() == 0 ? "未删除" : "已删除");
                    //是否收货(0:未收货/1:已收货)
                    orderPayedListVo.setOrderIfreceive(exOrder.getOrderIfreceive() == 0 ? "未收货" : "已收货");
                    //支付状态(0：未支付，1：已支付)
                    orderPayedListVo.setIfPay(exOrder.getOrderType() == 0 ? "未支付" : "已支付");
                    orderPayedListVos.add(orderPayedListVo);
                });
            }
            pageUtils.setCurrentList(orderPayedListVos);
            return pageUtils;
        } else {
            //商品主题不为空，从订单详情表中取数据
            //查询总条数
            Long totalCount = exOrderdetailMapper.queryAllItemsPerOrderCounts(queryOrderParam, pageUtils.getPageNo(), pageSize);
            pageUtils.setTotalCount(totalCount);
            //分页查询
            List<ExOrderdetail> exOrderdetails = exOrderdetailMapper.queryAllItemsPerOrder(queryOrderParam, pageUtils.getPageNo(), pageSize);
            //创建订单列表VO集合对象
            List<OrderPayedListVo> orderPayedListVos = new ArrayList<>();
            exOrderdetails.forEach(exOrderDetail -> {
                OrderPayedListVo orderPayedListVo = new OrderPayedListVo();
                BeanUtils.copyProperties(exOrderDetail, orderPayedListVo);
                ExItems exItems = exItemsMapper.selectByPrimaryKey(exOrderDetail.getItemId());
                //商品主图
                orderPayedListVo.setPicMain(exItems.getPicMain());
                //商品名称
                orderPayedListVo.setItemName(exItems.getItemName());
                //店铺名称
                ShopInfoExample shopInfoExample = new ShopInfoExample();
                shopInfoExample.createCriteria().andShopTypeEqualTo(Integer.valueOf(exOrderDetail.getShopId()));
                orderPayedListVo.setShopTypeName(shopInfoMapper.selectByExample(shopInfoExample).get(0).getShopTypename());
                //商品主题（0：上衣，1：裙装，2：短裤，3:女鞋...）
                ExItemtypeExample exItemtypeExample = new ExItemtypeExample();
                exItemtypeExample.createCriteria().andItemTypeEqualTo(exItems.getItemType());
                orderPayedListVo.setItemType(exItemtypeMapper.selectByExample(exItemtypeExample).get(0).getTypeName());
                ExOrder exOrder = exOrderMapper.selectByPrimaryKey(exOrderDetail.getOrderId());
                //是否超时(0：未超时，1：已超时)
                orderPayedListVo.setOrderOvertime(exOrder.getOrderOvertime() == 0 ? "未超时" : "已超时");
                //是否删除(0：未删除，1：已删除)假删除
                orderPayedListVo.setOrderIfdel(exOrder.getOrderIfdel() == 0 ? "未删除" : "已删除");
                //是否收货(0:未收货/1:已收货)
                orderPayedListVo.setOrderIfreceive(exOrder.getOrderIfreceive() == 0 ? "未收货" : "已收货");
                //支付状态(0：未支付，1：已支付)
                orderPayedListVo.setIfPay(exOrder.getOrderType() == 0 ? "未支付" : "已支付");
                orderPayedListVos.add(orderPayedListVo);
            });
            pageUtils.setCurrentList(orderPayedListVos);
            return pageUtils;
        }
    }


    /**
     * 删除订单
     *
     * @param userId
     * @return
     */
    public boolean deleteOrder(String orderId, String userId) {
        boolean flag = false;
        ExOrderExample exOrderExample = new ExOrderExample();
        exOrderExample.createCriteria().andOrderIdEqualTo(orderId).andUserIdEqualTo(userId);
        ExOrderdetailExample exOrderdetailExample = new ExOrderdetailExample();
        exOrderdetailExample.createCriteria().andOrderIdEqualTo(orderId).andUserIdEqualTo(userId);
        try {
            flag = exOrderMapper.deleteByExample(exOrderExample) > 0;
        } catch (Exception e) {
            log.error("删除订单失败！" + e);
            e.printStackTrace();
        }
        try {
            flag = exOrderdetailMapper.deleteByExample(exOrderdetailExample) > 0;
        } catch (Exception e) {
            log.error("删除订单失败！" + e);
            e.printStackTrace();
        }
        return flag;
    }

    public boolean ifExistUser(String userId) {
        ExUserExample exUserExample = new ExUserExample();
        exUserExample.createCriteria().andUserIdEqualTo(userId);
        if (exUserMapper.countByExample(exUserExample) > 0) {
            return true;
        }
        return false;
    }


}

