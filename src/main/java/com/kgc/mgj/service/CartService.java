package com.kgc.mgj.service;

import com.kgc.mgj.dto.ExCoupon;
import com.kgc.mgj.dto.ExItems;
import com.kgc.mgj.dto.ExUsercoupon;
import com.kgc.mgj.dto.ItemCartDto;
import com.kgc.mgj.mapper.CartMapper;
import com.kgc.mgj.mapper.ExCouponMapper;
import com.kgc.mgj.mapper.ExUsercouponMapper;
import com.kgc.mgj.utils.DateUtils;
import com.kgc.mgj.vo.CartCouponVo;
import com.kgc.mgj.vo.CartPageVo;
import com.kgc.mgj.vo.ItemCartVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 14:21
 */
@Slf4j
@Service
public class CartService {

    @Resource
    private CartMapper cartMapper;

    @Autowired
    private ExCouponMapper exCouponMapper;

    @Autowired
    private ExUsercouponMapper exUsercouponMapper;

    /**
     * 获取购物车页面信息
     * @param userId
     * @return
     */
    public CartPageVo getCartPage(String userId) {
        CartPageVo cartPageVo = new CartPageVo();
        //获取默认地址
        cartPageVo.setAddress(cartMapper.queryDefaultAddressByUserId(userId));
        //获取商品列表信息
        cartPageVo.setItemCartVos(getItemsInCart(userId));

        //计算优惠券的优惠金额 以及 使用数量
        double totalDiscountPrice = cartPageVo.getItemCartVos().stream().filter(itemCartVo -> null != itemCartVo.getCartCouponVo()).mapToDouble(itemCartVo->itemCartVo.getCartCouponVo().getUsedAmount()).sum();
        int totalDiscountNum = (int) cartPageVo.getItemCartVos().stream().filter(itemCartVo -> null != itemCartVo.getCartCouponVo()).count();
        cartPageVo.setUsedCouponNum(totalDiscountNum);
        cartPageVo.setDiscountPrice(totalDiscountPrice);
        //计算该购物车内商品一共多少钱
        double totalPrice = cartPageVo.getItemCartVos().stream().mapToDouble(ItemCartVo::getItemPrice).sum() - totalDiscountNum;
        cartPageVo.setTotalPrice(totalPrice);
        //计算购物车内一共有多少商品
        cartPageVo.setItemNum(cartPageVo.getItemCartVos().size());
        return cartPageVo;
    }

    /**
     * 获取购物车中的商品信息
     * @param userId
     * @return
     */
    private List<ItemCartVo> getItemsInCart(String userId) {
        //获取基本信息：itemId 、itemCount 、shopTypeName、 coupon_id ,这些可以直接在cart表中查到
        List<ItemCartVo> itemCartVos = cartMapper.queryItemsInCart(userId);
        //根据基本信息状态具体信息：item_name, pic_main, item_price, discount_price, item_detail
        itemCartVos.forEach(itemCartVo -> {
            ExItems exItems = cartMapper.queryItemInfoByItemId(itemCartVo.getItemId());
            //将 exItems中的属性复制到itemCartVo中 (复制时，同名的属性是会覆盖的)
            BeanUtils.copyProperties(exItems,itemCartVo);
            //将当前商品的库存返回给前端
            itemCartVo.setStock(cartMapper.queryStockByItemId(itemCartVo.getItemId()));
            //核对优惠券的有效性，如果无效，则删掉优惠券的展示（此处判断是否有效的方式仅仅为有没有过期）
            if (null != itemCartVo.getCartCouponVo()) {
                ExCoupon exCoupon = cartMapper.queryExcouponByCouponId(itemCartVo.getCartCouponVo().getCouponId());
                if (DateUtils.getDaysGapOfDates(exCoupon.getStartTime(), new Date()) < exCoupon.getValueTime()) {
                    CartCouponVo cartCouponVo = new CartCouponVo();
                    BeanUtils.copyProperties(exCoupon,cartCouponVo);
                    itemCartVo.setCartCouponVo(cartCouponVo);
                } else {
                    itemCartVo.setCartCouponVo(null);
                }
            }

        });
        return itemCartVos;
    }

    /**
     * 添加商品到购物车
     * @param itemCartDto
     * @return
     * 100 正常
     * 101 库存不足
     */
    public int addItemToCart(ItemCartDto itemCartDto) {
        //库存是否充足
        if (!checkStock(itemCartDto.getItemId(),itemCartDto.getItemCount())) {
            return 101;
        }

        //检查数据库中是否已经有该订单了，如果有，增加购买的量即可
        if (checkItemAlreadyInCartOrNot(itemCartDto.getUserId(),itemCartDto.getItemId(), itemCartDto.getItemCount(), itemCartDto.getCouponId())) {
            return 100;
        }

        //将商品信息入库
        cartMapper.insertItem(itemCartDto);

        //用户优惠券对应表插入
        ExUsercoupon exUsercoupon = new ExUsercoupon();
        exUsercoupon.setCouponId(itemCartDto.getCouponId());
        exUsercoupon.setCreateTime(new Date(System.currentTimeMillis()));
        exUsercoupon.setUseTime(new Date(System.currentTimeMillis()));
        exUsercoupon.setUserId(itemCartDto.getUserId());
        exUsercouponMapper.insertSelective(exUsercoupon);

        return 100;
    }

    /**
     * 检查数据库中是否已经有该商品了，如果有，增加购买的量即可
     *
     * @param itemId
     * @param itemCount
     */
    private boolean checkItemAlreadyInCartOrNot(String userId, String itemId, Integer itemCount, String couponId) {
        Integer itemCountInMysql = cartMapper.queryItemNumInCart(userId, itemId);
        if (null != itemCountInMysql) {
            cartMapper.updateItemCountByItemId(itemId, itemCount + itemCountInMysql, couponId);
            return true;
        }
        return false;
    }

    /**
     * 查看优惠券是否还有效
     * @param coupon
     * @return
     */
    private boolean checkCouponsIsActiveOrNot(String coupon) {
        ExCoupon exCoupon = cartMapper.queryExcouponByCouponId(coupon);
        return DateUtils.getDaysGapOfDates(exCoupon.getStartTime(), new Date()) < exCoupon.getValueTime();
    }

    /**
     * 查看库存是否充足
     * @param itemId
     * @param itemCount
     * @return
     */
    private boolean checkStock(String itemId,Integer itemCount) {
        int stock = cartMapper.queryStockFromItemRepoByItemId(itemId);
        return stock >= itemCount;
    }

    /**
     * 修改购物车中的商品数量
     * @param itemCartDto
     * @return
     * 100：操作正常
     * 101：库存不足，操作失败
     */
    public int modifyItemCount(ItemCartDto itemCartDto) {
        if (checkStock(itemCartDto.getItemId(), itemCartDto.getItemCount())) {
            cartMapper.updateItemCountByItemId(itemCartDto.getItemId(),itemCartDto.getItemCount(),itemCartDto.getCouponId());
            return 100;
        }
        return 101;
    }

    public int removeItemFromCart(String userId, String[] itemIds) {
        return cartMapper.deleteItemsByUserId(userId, Arrays.asList(itemIds));
    }
}
