package com.kgc.mgj.service;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.kgc.mgj.dto.Bank;
import com.kgc.mgj.dto.ExOrder;
import com.kgc.mgj.mapper.BankMapper;
import com.kgc.mgj.utils.RedisUtils;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jialin
 * @create 2020-10-08
 */
@Log4j
@Service
public class BankService {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private BankMapper bankMapper;

    public Integer BindBank(String userId, String card, String bankCode, String userName, String phoneCode) {
        Bank bank = new Bank();
        bank.setUserId(userId);
        bank.setUserCard(card);
        bank.setUserName(userName);
        bank.setUserBank(bankCode);
        String codeSave = redisUtils.get("phoneCode:" + userId).toString();
        if (!phoneCode.equals(codeSave)){
            log.error("验证码错误！");
            return 2;
        }
        try {
            bankMapper.insertSelective(bank);
            return 0;
        } catch (Exception e) {
            log.error("插入绑定银行卡信息失败！" + e);
            return 1;
        }
    }

    /**
     * 获取手机验证码
     *
     * @param phone
     */
    public void getPhoneCode(String userId, String phone) {
        //生成四位code
        String str = "";
        for (int i = 0; i < 4; i++) {
            str += (int) Math.floor(Math.random() * 10);
        }
        redisUtils.set("phoneCode:" + userId, str);
        sendMsg(phone, str);
    }

    /**
     * 发送手机验证码
     *
     * @param phone
     * @param str
     */
    public boolean sendMsg(String phone, String str) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4GGaGG3Uv6txBjPAfGGQ", "uWO2VIPLt7gP5Aywqep2xzK9sGbyg0");
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", "星空游戏商城");
        request.putQueryParameter("TemplateCode", "SMS_199216161");
        request.putQueryParameter("TemplateParam", "{'code':'" + str + "'}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            if (response.getData().contains("OK")) {
                return true;
            }
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }


}
