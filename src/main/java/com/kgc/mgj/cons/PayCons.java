package com.kgc.mgj.cons;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author jialin
 * @createTime 2020-09-17
 */
@Data
@Component
public class PayCons {
    private String payStatusNameSpace;
    private String pay;
}
