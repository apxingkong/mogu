package com.kgc.mgj.utils;

import lombok.Data;

import java.util.Collection;

/**
 * @author jialin
 */
@Data
public class PageUtils<T> {
    //当前页
    private int currentPage;
    //页码
    private int pageNo;
    //每页条数
    private Integer pageSize;
    //总条数
    private long totalCount;
    //总页数
    private long totalPage;
    //data
    private Collection<T> currentList;

    public int getPageNo() {
        //（当前页-1）*页面容量
        return (pageNo - 1) * pageSize;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public long getTotalPage() {
        return totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
    }

    public void setTotalPage(long totalPage){
        this.totalPage = totalPage;
    }
}
