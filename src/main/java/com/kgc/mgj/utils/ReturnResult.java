package com.kgc.mgj.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jialin
 */
@ApiModel("返回消息提示")
@Data
public class ReturnResult<T> {
    @ApiModelProperty("消息提示")
    private String msg;
    @ApiModelProperty("提示码")
    private int code;
    @ApiModelProperty("返回数据")
    private T data;
}
