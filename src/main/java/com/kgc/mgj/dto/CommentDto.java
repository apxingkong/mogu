package com.kgc.mgj.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CommentDto {

    private Integer commentid;


    private String userId;


    private String comment;


    private String picComment;


    private String itemId;


    private Integer evaluate;


    private Date createtime;



}