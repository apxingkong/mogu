package com.kgc.mgj.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ExItemsDto {
    /**
     * Database Column Remarks:
     *   商品id

     */
    private String itemId;

    /**
     * Database Column Remarks:
     *   商品名称

     */
    private String itemName;

    /**
     * Database Column Remarks:
     *   商品名称

     */
    private String itemDetail;

    /**
     * Database Column Remarks:
     *   商品主图

     */
    private String picMain;

    /**
     * Database Column Remarks:
     *   商品附图，详情页显示
     */
    private String picDetail;

    /**
     * Database Column Remarks:
     *   商品颜色(某些商品没有就空着)
     */
    private Integer itemColortype;

    /**
     * Database Column Remarks:
     *   商品尺码(某些商品没有就空着)

     */
    private Integer itemSizetype;

    /**
     * Database Column Remarks:
     *   商品原价

     */
    private Double itemPrice;

    /**
     * Database Column Remarks:
     *   商品折扣价

     */
    private Double discountPrice;

    /**
     * Database Column Remarks:
     *   商品主题（0：上衣，1：裙装，2：短裤，3:女鞋...）

     */
    private Integer itemType;

    /**
     * Database Column Remarks:
     *   热门品牌（0：库恩玛维，1：衣阁里拉...）

     */
    private Integer brandType;

    /**
     * Database Column Remarks:
     *   店铺编号

     */
    private String shopId;

    /**
     * Database Column Remarks:
     *   创建时间
     *

     */
    private Date createTime;

    /**
     * Database Column Remarks:
     *   修改时间

     */
    private Date updateTime;


}