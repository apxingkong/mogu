package com.kgc.mgj.dto;

import lombok.Data;

@Data
public class ExItemtypeDto {
    /**
     *      *  商品类型表主键id

     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   商品类型id

     * @mbg.generated
     */
    private Integer itemType;

    /**
     * Database Column Remarks:
     *   商品类型

     * @mbg.generated
     */
    private String typeName;

}