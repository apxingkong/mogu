package com.kgc.mgj.dto;

import lombok.Data;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/23 15:24
 * 入库的购物车模型
 */
@Data
public class ItemCartDto {
    private String userId;
    private String itemId;
    private Integer itemCount;
    //店铺名称
    private String shopTypeName;
    //优惠券id
    private String couponId;
}
