package com.kgc.mgj.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/23 14:55
 */
@Data
@ApiModel("添加进入购物车的商品入参信息")
public class AddToCartItemParam implements Serializable {
    @ApiModelProperty("添加到购物车的商品id")
    private String itemId;
    @ApiModelProperty("添加到购物车的商品数量")
    private Integer num;
    @ApiModelProperty("该商品的所属的店铺")
    private String shopTypeName;
    @ApiModelProperty("优惠券编号")
    private String couponId;
}
