package com.kgc.mgj.params;

/**
 * 定义优惠券单链表，管理最优优惠券
 *
 * @author jialin
 * @create 2020-10-09C
 */
public class CouponBestLinkedList {

    //初始化头节点，不动
    private CouponBestNode head = new CouponBestNode(0.0, "");

    /**
     * 按顺序插入最优优惠券节点
     *
     * @param couponBestNode
     */
    public void addBestCoupon(CouponBestNode couponBestNode) {
        //辅助节点，帮助找到添加的位子
        CouponBestNode temp = head;
        while (true) {
            if (temp.next == null) {
                break;
            }
            if (temp.next.bestPrice >= couponBestNode.bestPrice) {//位子找到，就在temp后面插入
                break;
            }
            temp = temp.next;//后移，遍历当前链表
        }
        //插入
        couponBestNode.next = temp.next;
        temp.next = couponBestNode;
    }

    /**
     * 返回第一个节点
     *
     * @return
     */
    public String getFirst() {
        return null == head.next ? "0" : head.next.couponId;
    }

}
