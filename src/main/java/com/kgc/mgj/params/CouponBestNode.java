package com.kgc.mgj.params;

/**
 * 定义最优优惠券对象，每个对象就是一个节点
 *
 * @author jialin
 * @create 2020-10-09
 */
public class CouponBestNode {

    public double bestPrice;
    public String couponId;
    public CouponBestNode next;

    public CouponBestNode(double bestPrice, String couponId) {
        this.bestPrice = bestPrice;
        this.couponId = couponId;
    }

    @Override
    public String toString() {
        return "couponBestNode{" +
                "bestPrice=" + bestPrice +
                ", couponId='" + couponId + '\'' +
                ", next=" + next +
                '}';
    }
}
