package com.kgc.mgj.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author jialin
 * @create 2020-10-08
 */
@Data
@ApiModel("查询订单的入参实体类")
public class QueryOrderParam implements Serializable {
    @ApiModelProperty("用户编号")
    private String userId;
    @ApiModelProperty("商品主题（0：上衣，1：裙装，2：短裤，3:女鞋...）")
    private Integer itemType;
    @ApiModelProperty("支付状态(0：未支付，1：已支付)")
    private Integer orderType;
    @ApiModelProperty("是否超时(0：未超时，1：已超时)")
    private Integer orderOvertime;
    @ApiModelProperty("是否删除(0：未删除，1：已删除)假删除")
    private Integer orderIfdel;
    @ApiModelProperty("是否收货(o未收货/1已收货)")
    private Integer orderIfreceive;
}
