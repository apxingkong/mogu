package com.kgc.mgj.mapper;

import com.kgc.mgj.dto.ItemColortype;
import com.kgc.mgj.dto.ItemColortypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ItemColortypeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    long countByExample(ItemColortypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int deleteByExample(ItemColortypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int insert(ItemColortype record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int insertSelective(ItemColortype record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    List<ItemColortype> selectByExample(ItemColortypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    ItemColortype selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") ItemColortype record, @Param("example") ItemColortypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") ItemColortype record, @Param("example") ItemColortypeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(ItemColortype record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_colorType
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(ItemColortype record);
}