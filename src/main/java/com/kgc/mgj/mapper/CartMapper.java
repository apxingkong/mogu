package com.kgc.mgj.mapper;

import com.kgc.mgj.dto.ExCoupon;
import com.kgc.mgj.dto.ExItems;
import com.kgc.mgj.dto.ItemCartDto;
import com.kgc.mgj.vo.ItemCartVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ：jacketzc
 * @date ：Created in 2020/9/22 14:23
 */
public interface CartMapper {

    String queryDefaultAddressByUserId(String userId);

    List<ItemCartVo> queryItemsInCart(String userId);

    ExItems queryItemInfoByItemId(String itemId);

    int queryStockByItemId(String itemId);

    ExCoupon queryExcouponByCouponId(String couponId);

    int queryStockFromItemRepoByItemId(String itemId);

    List<String> queryCouponsByItemId(String itemId);

    int queryItemInCartOrNot(String itemId);

    Integer queryItemNumInCart(@Param("userId") String userId,@Param("itemId") String itemId);

    void updateItemCountByItemId(@Param("itemId") String itemId, @Param("itemCount") int itemCount, @Param("couponId") String couponId);

    void insertItem(ItemCartDto itemCartDto);

    int deleteItemsByUserId(@Param("userId") String userId, @Param("itemIds") List<String> itemIds);
}
